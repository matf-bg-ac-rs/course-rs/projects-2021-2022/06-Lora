# Project Lora

Lora je kartaška igra za četiri osobe. Sastoji se iz osam podigara. Cilj je skupiti što manje poena u svim igrama. Igre su sledeće: 
1.  Herc (Treba izbegavati odnošenje karata koje su u hercu. Svaka odneta karta donosi +1 poen.)
2.  Dame (Treba izbegavati odnošenje dama. Svaka odneta Dama donosi +1 poen.)
3.  Što više (Treba odneti što više ruku. Svaka odneta ruka donosi -1 poen.)
4.  Što manje (Cilj je odneti što manje ruku . Svaka odneta ruka donosi+1 poen.)
5.  Žandar tref, poslednja ruka (Ne treba uzeti žandara (J) u trefu, niti poslednju ruku. I žandar tref i poslednja ruka donose po +4 poena.)
6.  Kralj herc, poslednja ruka (Ne treba uzeti kralja u hercu, niti poslednju ruku. I kralj herc i poslednja ruka donose po +4 poena.)
7.  Lora (Ovu igru počinje desni od delioca (u uobičajenom krugu). On stavlja neku kartu na sto. Sledeći trba da stavi ili prvu jaču kartu u istoj boji preko nje, ili istu kartu u drugoj boji pored nje. Kada se dodje do A, preko se stavlja 7. Ako igrač nema kartu koju će staviti, dobija tačku (+1 poen). Igrase završava kada neko od igrača uspe da spusti sve karte koje ima u rukama. Onaj ko je prvi završio, dobija -8+broj_tačaka poena, a ostali dobijaju samo +broj_tačaka poena.)
8.  Igra po izboru (U njoj, igrač koji sedi desno od delioca, bira koja će se od prethodnih 7 igara igrati ponovo.)

Kada se završi ceo krug (svih 8 igara), deljenje se prenosi na sledećeg igrača, i tako u krug, dok svi igrači ne završe sa deljenjem.

Lora se igra sa špilom od 32 karte (7, 8, 9, 10, J, Q, K, A). Karte poređane po jačini od najslabije do najjače: 7, 8, 9, 10, J, Q, K, A.


## Developers

- [Teodora Vasić, 1/2018](https://gitlab.com/tekisooj)
- [Tadej Gojić, 79/2018](https://gitlab.com/tadejgojic99)
- [Milica Kostadinović, 271/2018](https://gitlab.com/milica_k)
- [Ognjen Popović, 214/2018](https://gitlab.com/FoRPoP)
