#ifndef PRIKAZZAHTEVA_H
#define PRIKAZZAHTEVA_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class PrikazZahteva; }
QT_END_NAMESPACE

class PrikazZahteva : public QWidget
{
    Q_OBJECT

public:
    PrikazZahteva(QWidget *parent = nullptr);
    ~PrikazZahteva();

private:
    Ui::PrikazZahteva *ui;
};
#endif // PRIKAZZAHTEVA_H
