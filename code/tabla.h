#pragma once

#ifndef TABLA_H
#define TABLA_H

#include <QMainWindow>
#include <QImage>
#include <QLabel>
#include <QLCDNumber>
#include <QRadioButton>
#include <QPushButton>
#include <QTranslator>

#include "define.h"
#include "Game.h"
#include "Computer.h"
#include "help.h"
#include "stats.h"
#include "deck.h"
#include "config.h"
#include "positions.h"
#define MAX_PLAYER_NAMES 4

const int maxMainwindowHeight = 1000;
//const int DECK_SIZE           = 32;

QT_BEGIN_NAMESPACE
namespace Ui { class Tabla; }
QT_END_NAMESPACE

class Tabla : public QMainWindow{

    Q_OBJECT

public:
    Tabla(QWidget *parent = nullptr);
    ~Tabla();

    void initializeGraphics();
    void setInfoChannel();
    void clearDeck();
    void setCardsDisabled();
    void setLanguage(int lang);
private slots:
    void on_labelKartaURuci1_clicked();
    void on_labelKartaURuci2_clicked();
    void on_labelKartaURuci3_clicked();
    void on_labelKartaURuci4_clicked();
    void on_labelKartaURuci5_clicked();
    void on_labelKartaURuci6_clicked();
    void on_labelKartaURuci7_clicked();
    void on_labelKartaURuci8_clicked();
    void on_actionNew_triggered();
    void on_actionConnect_triggered();
    void on_actionRules_triggered();
    void on_actionReset_triggered();
    void on_actionShow_triggered();
    void connectedSetLights(bool connected);
    void on_actionEnglish_triggered();
    void on_actionSrpski_triggered();
    void on_lineEdit_returnPressed();
    void on_labelKartaNaTalonuDole_clicked();
    void on_labelKartaNaTalonuLevo_clicked();
    void on_labelKartaNaTalonuGore_clicked();
    void on_labelKartaNaTalonuDesno_clicked();
////    void on_actionTables_triggered();

private:
    Ui::Tabla *ui;
    QWidget* parent;
    QImage* imageEmptyCard;
    QImage* imageYourTurn;
    QImage* imageSitHere;
    QImage* imageConnected;
    QImage* imageDisconnected;
    QLabel* labels[18];
    QLCDNumber* lcdScore[4];
    QTranslator translator;

    Game* lora;
    Player* player;
    Config* config;
    Stats* stats;
    Deck* deck;
    //Client* client;
//    Tabla* tableList;
    QTimer* timer;
    Help* help;
    int cardHeight;
    int defCardPosY;


public slots:
    void message(QString mesg);
////    void refreshDeck(Player* plr, bool d);
////    void playCard(int card, int idx);
//    void clearTable();
//    void refreshScore(int score, int idx);
//    void showYourTurn(int idx);
//    void gameOver(int score1, int score2, int score3, int score4);
//    void joinGame(int id, char chair);
};
#endif // TABLA_H
