#ifndef GAME_H
#define GAME_H

#pragma once

#include <QObject>
#include <QList>
#include "player.h"
#include "deck.h"
#include "tabla.h"
#include "Card.h"
#include "talon.h"
#include <QTimer>

//const int backCard     = 100;
//const int yourTurn     = 255;

//const int NOERROR       = 0;
//const int ERRHEART      = 1;
//const int ERRSUIT       = 2;
//const int ERRQUEEN      = 3;


class Player;

class Game: public QObject{

    Q_OBJECT

public:

    enum class GAME{
        HEART = 0,
        QUEEN,
        LEAST,
        MOST,
        JACK_CLUB_LAST_HAND,
        KING_HEART_LAST_HAND,
        LORA,
        GAME_BY_CHOICE
    };
    Game();
    Game(QList<Player*> plyrs);
    void addPlayer(Player* plyr);
    void newGame(); //TODO
//        void check();
//        bool checkLora();
    void gameHeart();
    void gameQueen();
    void gameLess();
    void gameMore();
    void gameJackClubLastHand();
    void gameKingHeartLastHand();
    void gameLora();
    void gameByChoice();
    void play();


    Game::GAME getCurrentGame();
    bool isCardOnTable(Card* card);
    Card*  getLowestSuit(Player* plyr, Card::Suit suit);
    int  getLowestSuitPos(Player* plyr, Card::Suit suit);
    int  getCardPosition(Player* plyr, Card* card);
    Card*  getHighestSuit(Player* plr, Card::Suit suit);
    int  getHighestSuitPos(Player* plr, Card::Suit suit);
    Card*  getHighestCardTable();
    int  checkInvalidMove(Player* plr, Card* card);
    QVector<Card*> checkValidCards(Player* plr);
    int  evalCardStrength(Player* plr, Card* card);


    void initVars();
    void resetCardsOnTable();
    void resetScore();
    void resetCardsPlayed();
    void resetCardsLeftInSuit();
    void resetPlrHasCard();
    void randomDeck();
    void processCard(Card* card);
    void advanceTurn();
    void resetPlrCardsInSuit();

    bool isItDraw();
    bool isItMyTurn();
    bool isCardSelectable(Card* card);

    Player* whoAmI();
    Card::Suit getCurrentSuit();
    int  playCard(int idx);
    Card*  getCard(int plr, int idx);
    Card* getCard(Player* plr, int idx);
    Card*  getCard(int idx);
    int  getMyScore();
    int  countMyCards();
    int  countPlrCards(Player* plr);

    int  getPlrNameId(Player* plr);
    int  getLowestScore();
    int  getHighestScore();

   // void newGame();
    void sortPlrCards();
    QList<Player*> getPlayers();

private:

    QList<Player*> players;
    Talon* talon;
    QTimer* timer = new QTimer();
    bool cardsSelected[4][8];
    unsigned int userId;
    Player* plyrId[4];
    Player* user;
    Card::Suit currentSuit;
    Card* cardLeft;
    Game::GAME currentGame;

signals:
    void sigClearTable();
    void sigPlayCard(Card* card, int idx);
    void sigRefreshDeck(Player* plr, bool d);
    void sigScore(int score, int idx);
    void sigYourTurn(int idx);
    void sigGameOver(int score1, int score2, int score3, int score4);
};



#endif // GAME_H
