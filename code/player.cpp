#include "player.h"
Player::Player(){
    this->playerName = "";
    userId = -1;
}

Player::Player(QString name){
    this->playerName = name;
    pickedCard = nullptr;
}

void Player::pickCard(int pos){
    //TODO: izbor karte

    pickedCard = cardsInHand[pos];

}
Game::GAME Player::pickGame(){
    //TODO: izbor igre
}
void Player::takeCard(Card* card){
    cardsInHand.push_back(card);
    if(cardsInHand.size() == 8){
        sortCards();
    }
}
void Player::removeCard(Card* card){
    cardsInHand.removeOne(card);
}
void Player::addPoints(unsigned int pts){
    points += pts;
}
void Player::playCard(){
    /////////////////////////////
    /// OVDE DODATI DEO ZA ODABIR KARTE??????

}
QString Player::getPlayerName(){
    return playerName;
}
QVector<Card*> Player::getCardsInHand(){
    return cardsInHand;
}
unsigned int Player::getPoints(){
    return points;
}
//QString Player::getCardName(){

//}
bool Player::operator==(const Player* other){
    return this->playerName == other->playerName;
}
bool Player::operator!=(const Player* other){
    return this->playerName != other->playerName;
}

void Player::resetPoints(){
    points = 0;
}

void Player::reset(){
    points = 0;
    cardsInHand.clear();
}

bool Player::hasCard(Card* card){

    for(Card* c :cardsInHand){
        if(c == card)
            return true;
    }
    return false;
}

int compare(const void* a, const void* b){
    if(((Card*)a)->getCardSuit() == ((Card*)b)->getCardSuit()){
        return ((Card*)a)->getCardNumber() < ((Card*)b)->getCardNumber();
    }
    return static_cast<int>(((Card*)a)->getCardSuit()) < static_cast<int>(((Card*)b)->getCardSuit());
}

void Player::sortCards(){

    std::qsort(&cardsInHand, cardsInHand.size(), sizeof(Card*), compare);

}

void Player::setUserId(unsigned int id){
    userId = id;
}

unsigned int Player::getUserId(){
    return userId;
}
int Player::getNumOfCards(){
    return cardsInHand.size();
}

QVector<Card*> Player::getCardsWithSuit(Card::Suit s){

    QVector<Card*> cardsWithSuit;
    for(Card* card:getCardsInHand()){
        if(card->getCardSuit() == s)
            cardsWithSuit.append(card);
    }
    return cardsWithSuit;


}

Card* Player::getPickedCard(){
    return pickedCard;
}
void Player::resetPickedCard(){
    pickedCard = nullptr;
}
