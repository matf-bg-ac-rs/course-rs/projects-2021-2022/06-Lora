#include "Game.h"
#include <QObject>
#include <QPair>
#include "talon.h"
#include <QTimer>
Game::Game(){
    talon = new Talon();

}
Game::Game(QList<Player*> plyrs){
    players = plyrs;
    talon = new Talon();
    currentGame = Game::GAME::HEART;
}
void Game::addPlayer(Player *plyr){
    players.push_back(plyr);
    plyr->setUserId(players.size());
}

Game::GAME Game::getCurrentGame() {
    return currentGame;
}


 void Game::gameHeart(){
    currentGame = Game::GAME::HEART;
    for(int i = 0; i < 8; ++i){
        for(int j = 0; j < 4; ++j) {
            timer->start(10000);
            Card* selectedCard = players[j]->getPickedCard();


            talon->addCard(selectedCard, players[j]);
//            players.front()->removeCard(players.front()->getCardsInHand()[i]);
            players[j]->resetPickedCard();
            players[j]->removeCard(selectedCard);
        }
        int pts = talon->countCardsWithSuit(Card::Suit::HEART);

        Card::Suit suit = talon->getTalonSuit();
        QPair<Card*,Player*> played = talon->takeFromTop();
        unsigned int strongestCard = played.first->getCardNumber();
        Player* playerWithStrongestCard = played.second;

        while(!talon->empty()){
            played = talon->takeFromTop();
            if(played.first->getCardSuit() == suit && played.first->getCardNumber() > strongestCard){
                strongestCard = played.first->getCardNumber();
                playerWithStrongestCard = played.second;
            }
        }

        playerWithStrongestCard->addPoints(pts);
        while(players.front() != playerWithStrongestCard){
            players.push_back(players.front());
            players.pop_front();
        }

    }

}
void Game::gameQueen(){
    currentGame = Game::GAME::QUEEN;
    for(int i = 0; i < 8; ++i){
        for(int j = 0; j < 4; ++j) {
            timer->start(10000);
            Card* selectedCard = players[j]->getPickedCard();

            talon->addCard(selectedCard, players[j]);
//            players.front()->removeCard(players.front()->getCardsInHand()[i]);
            players[j]->resetPickedCard();
            players[j]->removeCard(selectedCard);
        }
        int pts = talon->countCardsWithNumber(Card::Number::QUEEN);

        Card::Suit suit = talon->getTalonSuit();
        QPair<Card*, Player*> played = talon->takeFromTop();
        unsigned int strongestCard = played.first->getCardNumber();
        Player* playerWithStrongestCard = played.second;

        while(!talon->empty()){
            played = talon->takeFromTop();
            if(played.first->getCardSuit() == suit && played.first->getCardNumber() > strongestCard){
                strongestCard = played.first->getCardNumber();
                playerWithStrongestCard = played.second;
            }
        }

        playerWithStrongestCard->addPoints(pts);
        while(players.front() != playerWithStrongestCard){
            players.push_back(players.front());
            players.pop_front();
        }
    }
}
void Game::gameLess(){
    currentGame = Game::GAME::LEAST;
    for(int i = 0; i < 8; ++i){
        for(int j = 0; j < 4; ++j) {
            timer->start(10000);
            Card* selectedCard = players[j]->getPickedCard();

            talon->addCard(selectedCard, players[j]);
//            players.front()->removeCard(players.front()->getCardsInHand()[i]);
            players[j]->resetPickedCard();
            players[j]->removeCard(selectedCard);

        }

        Card::Suit suit = talon->getTalonSuit();
        QPair<Card*, Player*> played = talon->takeFromTop();
        unsigned int strongestCard = played.first->getCardNumber();
        Player* playerWithStrongestCard = played.second;

        while(!talon->empty()){
            played = talon->takeFromTop();
            if(played.first->getCardSuit() == suit && played.first->getCardNumber() > strongestCard){
                strongestCard = played.first->getCardNumber();
                playerWithStrongestCard = played.second;
            }
        }

        while(players.front() != playerWithStrongestCard){
            players.push_back(players.front());
            players.pop_front();
        }

        playerWithStrongestCard->addPoints(1);
        while(players.front() != playerWithStrongestCard){
            players.push_back(players.front());
            players.pop_front();
        }
    }
}
void Game::gameMore(){
    currentGame = Game::GAME::MOST;
    for(int i = 0; i < 8; ++i){
        for(int j = 0; j < 4; ++j) {
            timer->start(10000);
            Card* selectedCard = players[j]->getPickedCard();
            talon->addCard(selectedCard, players[j]);
//            players.front()->removeCard(players.front()->getCardsInHand()[i]);
            players[j]->resetPickedCard();
            players[j]->removeCard(selectedCard);

        }

        Card::Suit suit = talon->getTalonSuit();
        QPair<Card*, Player*> played = talon->takeFromTop();
        unsigned int strongestCard = played.first->getCardNumber();
        Player* playerWithStrongestCard = played.second;

        while(!talon->empty()){
            played = talon->takeFromTop();
            if(played.first->getCardSuit() == suit && played.first->getCardNumber() > strongestCard){
                strongestCard = played.first->getCardNumber();
                playerWithStrongestCard = played.second;
            }
        }

        while(players.front() != playerWithStrongestCard){
            players.push_back(players.front());
            players.pop_front();
        }

        playerWithStrongestCard->addPoints(-1);
        while(players.front() != playerWithStrongestCard){
            players.push_back(players.front());
            players.pop_front();
        }
    }
}
void Game::gameJackClubLastHand(){
    currentGame = Game::GAME::JACK_CLUB_LAST_HAND;
    for(int i=0; i<8; ++i){
        for(int j=0; j<4; ++j){
            timer->start(10000);
            Card* selectedCard = players[j]->getPickedCard();
            talon->addCard(selectedCard, players[j]);
//            players.front()->removeCard(players.front()->getCardsInHand()[i]);
            players[j]->resetPickedCard();
            players[j]->removeCard(selectedCard);

        }
        Card::Suit suit = talon->getTalonSuit();
        QPair<Card*, Player*> played = talon->takeFromTop();
        unsigned int strongestCard = played.first->getCardNumber();
        Player* playerWithStrongestCard =played.second;
        bool jackClub = false;
        while(!talon->empty()){
            played = talon->takeFromTop();
            if(played.first->getCardSuit() == suit && played.first->getCardNumber() > strongestCard){
                strongestCard = played.first->getCardNumber();
                playerWithStrongestCard =played.second;
            }
            if(played.first->getCardSuit() == Card::Suit::CLUB && played.first->getCardNumber() == static_cast<unsigned int>(Card::Number::JACK)){
                jackClub = true;
            }
        }
        while(players.front() != playerWithStrongestCard){
            players.push_back(players.front());
            players.pop_front();
        }
        if(jackClub){
            playerWithStrongestCard->addPoints(4);
            while(players.front() != playerWithStrongestCard){
                players.push_back(players.front());
                players.pop_front();
            }
        }
        if(i == 3){
            playerWithStrongestCard->addPoints(4);
            while(players.front() != playerWithStrongestCard){
                players.push_back(players.front());
                players.pop_front();
            }
        }

    }
}
void Game::gameKingHeartLastHand(){
    currentGame = Game::GAME::KING_HEART_LAST_HAND;
    for(int i=0; i<8; ++i){
        for(int j=0; j<4; ++j){
            timer->start(10000);
            Card* selectedCard = players[j]->getPickedCard();
            talon->addCard(selectedCard, players[j]);
//            players.front()->removeCard(players.front()->getCardsInHand()[i]);
            players[j]->resetPickedCard();
            players[j]->removeCard(selectedCard);

        }
        Card::Suit suit = talon->getTalonSuit();
        QPair<Card*, Player*> played = talon->takeFromTop();
        unsigned int strongestCard = played.first->getCardNumber();
        Player* playerWithStrongestCard =played.second;
        bool kingHeart = false;
        while(!talon->empty()){
            played = talon->takeFromTop();
            if(played.first->getCardSuit() == suit && played.first->getCardNumber() > strongestCard){
                strongestCard = played.first->getCardNumber();
                playerWithStrongestCard =played.second;
            }
            if(played.first->getCardSuit() == Card::Suit::HEART && played.first->getCardNumber() == static_cast<unsigned int>(Card::Number::KING)){
                kingHeart = true;
            }
        }
        //////////////////////////
        while(players.front() != playerWithStrongestCard){
            players.push_back(players.front());
            players.pop_front();
        }
        /////////////////////////

        if(kingHeart){
            playerWithStrongestCard->addPoints(4);
            while(players.front() != playerWithStrongestCard){
                players.push_back(players.front());
                players.pop_front();
            }
        }
        if(i == 3){
            playerWithStrongestCard->addPoints(4);
            while(players.front() != playerWithStrongestCard){
                players.push_back(players.front());
                players.pop_front();
            }
        }

    }
}
void Game::gameLora(){
            currentGame = Game::GAME::LORA;
//          Talon talonHeart = Talon();
//          Talon talonSpade = Talon();
//          Talon talonDiamond = Talon();
//          Talon talonClub = Talon();

//          QVector<Talon> taloni{talonHeart, talonSpade, talonDiamond, talonClub};

//          bool krajIgre = false;
//          while(!krajIgre){
//              for(int j = 0; j < 4; ++j){
//                  if (checkLora()){
//    timer->start(10000);
//    Card* selectedCard = players[j]->getPickedCard();
//    players[j]->resetPickedCard();
//    players[j]->removeCard(selectedCard);
//                      Card::Suit selectedCardSuit = players.front().getCardsInHand()[selectedCard].getTalonSuit();
//                      unsigned int selectedCardNumber = players.front().getCardsInHand()[selectedCard].getCardNumber();

//                      bool validCard = false;
//                      while (!validCard){
//                          for (unsigned int brojTalona = 0; brojTalona < 4; ++brojTalona){
//                              if (!taloni[brojTalona].empty() && taloni[brojTalona].getTalonSuit() == selectedCardSuit){
//                                  if (taloni[brojTalona].getCardNumber() == static_cast<unsigned int>(Card::Number::KING) && selectedCardNumber == static_cast<unsigned int>(Card::Number::SEVEN)){
//                                      taloni[brojTalona].addCard(&players.front().getCardsInHand()[selectedCard], &players[j]);
//                                      validCard = true;
//                                      break;
//                                  }

//                                  if (taloni[brojTalona].getCardNumber() == selectedCardNumber - 1){
//                                      taloni[brojTalona].addCard(&players.front().getCardsInHand()[selectedCard], &players[j]);
//                                      validCard = true;
//                                      break;
//                                  }
//                              }
//                          }

//                          switch (selectedCardSuit){

//                              case Card::Suit::TREF:
//                                  if (talonClub.empty()){
//                                      talonClub.addCard(&players.front().getCardsInHand()[selectedCard], &players[j]);
//                                      validCard = true;
//                                      break;
//                                  }

//                              case Card::Suit::PIK:
//                                  if (talonSpade.empty()){
//                                      talonSpade.addCard(&players.front().getCardsInHand()[selectedCard], &players[j]);
//                                      validCard = true;
//                                      break;
//                                  }

//                              case Card::Suit::HERC:
//                                  if (talonHeart.empty()){
//                                      talonHeart.addCard(&players.front().getCardsInHand()[selectedCard], &players[j]);
//                                      validCard = true;
//                                      break;
//                                  }

//                              case Card::Suit::KARO:
//                                  if (talonDiamond.empty()){
//                                      talonDiamond.addCard(&players.front().getCardsInHand()[selectedCard], &players[j]);
//                                      validCard = true;
//                                      break;
//                                  }
//                          }

//                          if (!validCard){
//                              std::cout << "Izabrali ste nekorektnu kartu! Izaberite drugu." << std::endl;
//                              std::cin >> selectedCard;
//                              selectedCardSuit = players.front().getCardsInHand()[selectedCard].getTalonSuit();
//                              selectedCardNumber = players.front().getCardsInHand()[selectedCard].getCardNumber();
//                          }
//                      }

//                      if (players[j].getCardsInHand().empty()){
//                          players[j].addPoints(-8);
//                          krajIgre = true;
//                          break;
//                      }
//                  }
//                  else{
//                      players[j].addPoints(1);
//                  }
//              }
//          }
}
void Game::gameByChoice(){
    currentGame = Game::GAME::GAME_BY_CHOICE;
    Game::GAME choice = players.front()->pickGame();
//          std::cout << "Izaberite neku od igara(0-6): " << std::endl;
//          std::cin >> choice;
//    HEART = 0,
//    QUEEN,
//    LEAST,
//    MOST,
//    JACK_CLUB_LAST_HAND,
//    KING_HEART_LAST_HAND,
//    LORA,
//    GAME_BY_CHOICE

    switch (choice) {
        case Game::GAME::HEART:
            gameHeart();
            break;
        case Game::GAME::QUEEN:
            gameQueen();
            break;
        case Game::GAME::LEAST :
            gameLess();
            break;
        case Game::GAME::MOST:
            gameMore();
            break;
        case Game::GAME::JACK_CLUB_LAST_HAND:
            gameJackClubLastHand();
            break;
        case Game::GAME::KING_HEART_LAST_HAND:
            gameKingHeartLastHand();
            break;
        case Game::GAME::LORA:
            gameLora();
            break;
        default: {
            printf("Greska, uneti neku od igara\n");
            gameByChoice();
        }
    }
}
void Game::play(){

    Deck* deck = new Deck();
    QList<Player*> nowDeals = players;

    for(unsigned int i=0; i<4; ++i){

        deck->dealCards(nowDeals);
        gameHeart();
        nowDeals = players;
        deck->dealCards(nowDeals);
        gameQueen();
        nowDeals = players;
        deck->dealCards(nowDeals);
        gameLess();
        nowDeals = players;
        deck->dealCards(nowDeals);
        gameMore();
        nowDeals = players;
        deck->dealCards(nowDeals);
        gameJackClubLastHand();
        nowDeals = players;
        deck->dealCards(nowDeals);
        gameKingHeartLastHand();
        nowDeals = players;
        deck->dealCards(nowDeals);
        gameLora();
        nowDeals = players;
        deck->dealCards(nowDeals);
        gameByChoice();
        players.push_back(players.front());
        players.pop_front();
        nowDeals = players;

    }
    //azuriranje rezultata u tabeli

}


void Game::newGame()
{
    initVars();

    resetScore();

    resetCardsOnTable();
    resetCardsLeftInSuit();
    resetCardsPlayed();
    resetPlrCardsInSuit();
    resetPlrHasCard();

    randomDeck();
    userId = user->getUserId();

    emit sigClearTable();
    emit sigRefreshDeck(user, true);
}

void Game::initVars()
{
    for(int i=0;i<4;++i){
        for(int j=0;j<8;++j){
            cardsSelected[i][j] = false;
        }
    }

}

void Game::resetScore()
{
    for (int i=0; i<4; i++) {
        players[i]->resetPoints();

        emit sigScore(players[i]->getPoints(), i);
    }
}


void Game::randomDeck()
{

    Deck* deck = new Deck();
    deck->dealCards(players);
    sortPlrCards();
    delete deck;
}


void Game::sortPlrCards() {

    for(Player* plyr:players){
        plyr->sortCards();
    }

}


void Game::advanceTurn()
{
    players.push_back(players.first());


    emit sigClearTable();


}

int Game::checkInvalidMove(Player* plr, Card* card) {
    if(checkValidCards(plr).indexOf(card) == -1)
        return 0;
    return 1;
}

QVector<Card*> Game::checkValidCards(Player* plr){
    if(Game::GAME::LORA == currentGame){
        //TODO

    }
    QVector<Card*> cardsWithSuit = plr->getCardsWithSuit(talon->getTalonSuit());

    if(cardsWithSuit.size() == 0)
        return plr->getCardsInHand();

    return cardsWithSuit;

}


//int Game::playCard(int idx)
//{
//  Card* card = players[userId]->getCardsInHand()[idx];

//  int check = checkInvalidMove(players[userId], card);

//  if (check != NOERROR)
//    return check;

//  currentSuit = card->getCardSuit();
//  players[userId]->removeCard(card);
//  processCard(card);
//  sortPlrCards();

//  emit sigRefreshDeck(players[userId], false);
//  emit sigPlayCard(card, userId);

//  advanceTurn();

//  return check;
//}


bool Game::isItDraw()
{
    for(int i=0;i<4;++i){
        for(int j=i+1; j<4; ++j){
            if(players[i]->getPoints() == players[j]->getPoints())
                return true;
        }
    }
    return false;
}

bool Game::isCardSelectable(Card* card)
{
    if (players.first() != user)
        return false;

    return (checkInvalidMove(user, card));
}

Card* Game::getLowestSuit(Player* plr, Card::Suit suit)
{
    for(Card* card:plr->getCardsInHand()){
        if(card->getCardSuit() == suit)
            return card;
    }
    return nullptr;
}

int Game::getLowestSuitPos(Player* plr, Card::Suit suit)
{
    int i=0;
    for(Card* card:plr->getCardsInHand()){
        if(card->getCardSuit() == suit)
            return i;
        i++;
    }
    return -1;
}

int Game::getHighestSuitPos(Player* plr, Card::Suit suit)
{
    int i=0;
    bool found=false;
    for(Card* card:plr->getCardsInHand()){
        if(card->getCardSuit() == suit)
            found = true;
        else if(card->getCardSuit() != suit && !found){
            return i;
        }
        ++i;
    }
    return -1;
}

Card* Game::getHighestSuit(Player* plr, Card::Suit suit)
{
    Card* c = nullptr;
    for(Card* card:plr->getCardsInHand()){
        if(card->getCardSuit() == suit){
            c = card;
        }

    }
    return c;
}

int Game::countMyCards(){
    return user->getNumOfCards();
}
int Game::countPlrCards(Player* plyr)
{
    return plyr->getNumOfCards();
}

int Game::getCardPosition(Player* plr, Card* card)
{
    int i=0;
    for(Card* c:plr->getCardsInHand()){
        if(c == card){
            return i;
        }
        ++i;
    }
    return -1;
}




int Game::getLowestScore()
{
    unsigned int score = static_cast<int>(INT_MAX);
    for(Player* plr : players){
        if(plr->getPoints() < score)
            score = plr->getPoints();
    }
    return score;
}

int Game::getHighestScore()
{
    unsigned int score = 0;
    for(Player* plr : players){
        if(plr->getPoints() > score)
            score = plr->getPoints();
    }
    return score;
}



bool Game::isItMyTurn()
{
    return players.front() == user;
}


Card::Suit Game::getCurrentSuit()
{
    return currentSuit;
}

int Game::getMyScore()
{
    for(Player* plr:players){
        if(plr == user)
            return plr->getPoints();
    }
}




Player* Game::whoAmI(){
    return user;
}

Card* Game::getCard(int plr, int idx)
{
    return players[plr]->getCardsInHand()[idx];
}

Card* Game::getCard(Player* plr, int idx)
{
    return plr->getCardsInHand()[idx];
}


Card* Game::getCard(int idx)
{
    return user->getCardsInHand()[idx];

}

QList<Player*> Game::getPlayers(){
    return players;
}
