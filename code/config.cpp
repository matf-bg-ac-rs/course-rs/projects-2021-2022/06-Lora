#include <QFile>
#include <QDir>
#include <QString>
#include <QTextStream>
#include "config.h"
#include "define.h"
#include "deck.h"
#include <QTranslator>
Config::Config()
{
 initVars();

 QFile file(QDir::homePath() + CONFIG_FILENAME);

 if (!file.exists())
   saveConfigFile();      // create the file by saving the default values.
 else
   loadConfigFile();
}

Config::~Config()
{
}

void Config::initVars()
{
  autoCentering = true;
//  cheat_mode = false;
  infoChannel = true;
//  sounds = true;
  detectTram = true;
  easyCardSelection = true;
  saveGame = true;

//  perfect_100 = false;
//  omnibus = false;
//  queen_spade_break_heart = false;
//  no_trick_bonus = false;
//  new_moon = false;
//  no_draw = false;

  username = "";
  password = "";
  warning = true;

  language = LANG_ENGLISH;
//  deckStyle = STANDARD_DECK;
}

int Config::loadConfigFile() {
  QFile file(QDir::homePath() + CONFIG_FILENAME);

  int cpt = 0;

  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    return ERROPENRO;
  }

  while (!file.atEnd()) {
      cpt++;
      QString line = file.readLine();
      QString param = line.section(" ", 0, 0);
      QString value = line.section(" ", -1, -1).simplified();

      if (param == "Language") {
        if (value == "english")
          language = LANG_ENGLISH;
        else
        if (value == "french")
          language = LANG_FRENCH;
        else
        if (value == "russian")
          language = LANG_RUSSIAN;
        else
        if(value == "serbian")
            language = LANG_SERBIAN;
        continue;
      }

//      if (param == "Deck_Style") {
//        if (value == "standard")
//          deck_style = STANDARD_DECK;
//        else
//        if (value == "english")
//          deck_style = ENGLISH_DECK;
//        else
//        if (value == "russian")
//          deck_style = RUSSIAN_DECK;

//        continue;
//      }

      if (param == "Username") {
        username  = value;
        continue;
      }

      if (param == "Password") {
        password = value;
        continue;
      }

      bool enable;

      if (value == "true")
        enable = true;
      else
        enable = false;

      if (param == "Auto_Centering")
        autoCentering = enable;

      else
      if (param == "Info_Channel")
        infoChannel = enable;
      else
      if (param == "Sounds")
        sounds = enable;
      else
      if (param == "Detect_Tram")
        detectTram = enable;
      else
      if (param == "Easy_Card_Selection")
        easyCardSelection = enable;
      else
      if (param == "Save_Game")
        saveGame = enable;
      if (param == "Warning")
        warning = enable;
      else {
          // unknown param
      }

      if (cpt > 18) break; // too many lines ?? corrupted file ??
  }
  file.close();

  return FNOERR;
}

int Config::setLanguage(int lang) {
  language = lang;

  return saveConfigFile();
}

//int Config::setDeckStyle(int style) {
//  deckStyle = style;

//  return save_config_file();
//}

int Config::setConfigFile(int param, bool enable)
{
  switch (param) {
    case CONFIG_AUTO_CENTERING :          autoCentering = enable; break;
    case CONFIG_INFO_CHANNEL :            infoChannel = enable; break;
    case CONFIG_SOUNDS :                  sounds = enable; break;
    case CONFIG_DETECT_TRAM :             detectTram = enable; break;
    case CONFIG_SAVE_GAME :               saveGame = enable; break;
    case CONFIG_EASY_CARD_SELECTION :     easyCardSelection = enable; break;
    case CONFIG_WARNING :                 warning = enable; break;
  }

  return saveConfigFile();
}

int Config::saveConfigFile()
{
  QFile file(QDir::homePath() + CONFIG_FILENAME);

  if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
     return ERROPENWO;
  }

  QTextStream out(&file);

  if (username.size() && password.size()) {
    out << "Username = " << username << '\n';
    out << "Password = " << password << '\n';
    out << "Warning = " << (warning ? "true" : "false") << '\n';
  }

  switch (language) {
    case LANG_ENGLISH: out << "Language = english" << '\n'; break;
    case LANG_FRENCH:  out << "Language = french"  << '\n'; break;
    case LANG_RUSSIAN: out << "Language = russian" << '\n'; break;
    case LANG_SERBIAN: out << "Language = serbian" << '\n'; break;
  }

//  switch (deck_style) {
//    case STANDARD_DECK: out << "Deck_Style = standard" << '\n'; break;
//    case ENGLISH_DECK:  out << "Deck_Style = english" << '\n'; break;
//    case RUSSIAN_DECK:  out << "Deck_Style = russian" << '\n'; break;
//  }

  out << "Auto_Centering = " << (autoCentering ? "true" : "false") << '\n';
  out << "Info_Channel = " << (infoChannel ? "true" : "false") << '\n';
  out << "Sounds = " << (sounds ? "true" : "false") << '\n';
  out << "Detect_Tram = " << (detectTram ? "true" : "false") << '\n';
  out << "Easy_Card_Selection = " << (easyCardSelection ? "true" : "false") << '\n';

  out << "Save_Game = " << (saveGame ? "true" : "false") << '\n';

  file.close();
  return FNOERR;
}

void Config::setOnline(QString u, QString p)
{
  username = u;
  password = p;

  saveConfigFile();
}

QString &Config::Username()
{
  return username;
}

QString &Config::Password()
{
  return password;
}

bool Config::Warning()
{
  return warning;
}

bool Config::isAutoCentering() {
  return autoCentering;
}



bool Config::isInfoChannel() {
  return infoChannel;
}

bool Config::isSounds() {
  return sounds;
}

bool Config::isDetectTram() {
  return detectTram;
}

bool Config::isSaveGame() {
  return saveGame;
}

bool Config::isEasyCardSelection() {
  return easyCardSelection;
}

int Config::getLanguage() {
  return language;
}

//int Config::get_deck_style() {
//  return deck_style;
//}
