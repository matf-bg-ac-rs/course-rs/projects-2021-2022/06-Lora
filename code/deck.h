#pragma once
#ifndef DECK_H
#define DECK_H

#include <QObject>
#include <QList>
#include <QVector>
#include "Card.h"
#include "player.h"
class Deck : public QObject
{
    Q_OBJECT

public:
    Deck();
    unsigned int getNumberOfCards();
    bool empty();
    Card* takeCard();
    void dealCards(QList<Player*> players);
    void shuffleDeck();
    QImage* getCardImage(int card);

private:
    unsigned int numberOfCards;
    QVector<Card*> deck;
    QImage* cardsImages[DECK_SIZE];
    QImage* emptyCardImage;
    QImage* imageYourTurn;
    QImage* imageBackCard;
};

#endif // DECK_H
