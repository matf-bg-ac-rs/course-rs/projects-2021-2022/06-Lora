#ifndef CONFIG_H
#define CONFIG_H

#include <QString>

const int CONFIG_AUTO_CENTERING          = 0;
const int CONFIG_CHEAT_MODE              = 1;
const int CONFIG_INFO_CHANNEL            = 2;
const int CONFIG_SOUNDS                  = 3;
const int CONFIG_DETECT_TRAM             = 4;
//const int CONFIG_PERFECT_100             = 5;
//const int CONFIG_OMNIBUS                 = 6;
//const int CONFIG_QUEEN_SPADE_BREAK_HEART = 7;
//const int CONFIG_NO_TRICK_BONUS          = 8;
//const int CONFIG_NEW_MOON                = 9;
const int CONFIG_NO_DRAW                 = 10;
const int CONFIG_SAVE_GAME               = 11;
const int CONFIG_LANGUAGE                = 12;
const int CONFIG_EASY_CARD_SELECTION     = 13;
const int CONFIG_DECK_STYLE              = 14;
const int CONFIG_USERNAME                = 15;
const int CONFIG_PASSWORD                = 16;
const int CONFIG_WARNING                 = 17;

const char CONFIG_FILENAME[10]  = "/.hearts";

class Config
{
public:
    Config();
    ~Config();

private:
    void initVars();
    int saveConfigFile();
    int loadConfigFile();

    int language;
    int deckStyle;

    // game variants
//    bool perfect_100;
//    bool omnibus;
//    bool queen_spade_break_heart;
//    bool no_trick_bonus;
//    bool new_moon;
//    bool no_draw;

    // settings
    bool autoCentering;
//    bool cheatMode;
    bool infoChannel;
    bool sounds;
    bool detectTram;
    bool easyCardSelection;
    bool saveGame;

    // online
    QString username;
    QString password;
    bool warning;

public:
    QString &Username();
    QString &Password();
    bool Warning();

    void setOnline(QString u, QString p);
    int setConfigFile(int param, bool enable);
    int setLanguage(int lang);
    int setDeckStyle(int style);
    int getLanguage();
    int getDeckStyle();

    bool isAutoCentering();
//    bool isCheatMode();
    bool isInfoChannel();
    bool isSounds();
    bool isDetectTram();

//    bool is_perfect_100();
//    bool is_omnibus();
//    bool is_queen_spade_break_heart();
//    bool is_no_trick_bonus();
//    bool is_new_moon();
//    bool is_no_draw();
    bool isSaveGame();
    bool isEasyCardSelection();
};

#endif // CONFIG_H
