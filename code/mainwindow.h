#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "povezivanjeigraca.h"
#include "help.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setPovezivanjeIgraca(PovezivanjeIgraca* povezivanje);

private slots:

    void on_pushButtonRacunar_clicked();

    void on_pushButtonIgraci_clicked();

    void on_pushButtonIgraj_clicked();

private:
    Ui::MainWindow *ui;
    PovezivanjeIgraca* povezivanjeIgraca;
    Help* help;
    void on_menuHelp_clicked();
};
#endif // MAINWINDOW_H
