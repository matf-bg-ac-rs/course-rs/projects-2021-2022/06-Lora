#include "talon.h"
#include <QPair>

QList<QPair<Card*, Player*>> Talon::getCardsOnTable(){
    return cardsOnTable;
}
void Talon::addCard(Card* c, Player* plyr){
    QPair<Card*, Player*> pair;
    pair.first = c;
    pair.second = plyr;
    cardsOnTable.push_back(pair);
}
int Talon::countCardsWithNumber(Card::Number num){
    int count = 0;
    for(auto el:cardsOnTable){
        if(el.first->getCardNumber() == static_cast<unsigned int>(num))
            count++;
    }
    return count;
}
int Talon::countCardsWithSuit(Card::Suit s){
    int count = 0;
    for(auto el:cardsOnTable){
        if(el.first->getCardSuit() == s)
            count++;
    }
    return count;
}
QPair<Card*, Player*> Talon::takeFromTop(){
    return cardsOnTable.takeLast();
}
Card::Suit Talon::getTalonSuit(){
    return cardsOnTable.begin()->first->getCardSuit();
}
bool Talon::empty(){
    return cardsOnTable.size() == 0;
}
