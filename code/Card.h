#ifndef CARD_H
#define CARD_H

#include <QObject>

class Card:public QObject
{
    Q_OBJECT

public:
    enum class Suit{
        CLUB = 0,
        DIAMOND,
        HEART,
        SPADE
    };

    enum class Number{
        SEVEN = 0,
        EIGHT,
        NINE,
        TEN,
        JACK,
        QUEEN,
        KING,
        ACE
    };
    Card(Card::Number num, Card::Suit s);
    unsigned int getCardNumber();
    Card::Suit getCardSuit();

private:
    Card::Suit suit;
    Card::Number number;


};



#endif // CARD_H
