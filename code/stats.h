#ifndef STATS_H
#define STATS_H

#include <QWidget>
#include <QTableWidgetItem>

#include "define.h"
#include "ui_stats.h"
const char STATS_FILENAME[20] = "/.hearts.stats";
const char STATS_BACKUP_FILE[20] = "/.hearts.stats.bak";

const int STATS_GAME_STARTED  = 0;
const int STATS_GAME_FINISHED = 1;
const int STATS_HANDS_PLAYED  = 2;
const int STATS_SCORES        = 3;
const int STATS_FIRST_PLACE   = 4;
const int STATS_SECOND_PLACE  = 5;
const int STATS_THIRD_PLACE   = 6;
const int STATS_FOURTH_PLACE  = 7;

namespace Ui { class Stats; }

class Stats : public QWidget
{
    Q_OBJECT

public:
    explicit Stats(QWidget *parent = nullptr);
    ~Stats();

private:
    Ui::Stats *ui;
    bool fileCorrupted;

    QTableWidgetItem *item_names[MAX_PLR_NAMES];
    QTableWidgetItem *item_firstPlace[MAX_PLR_NAMES];
    QTableWidgetItem *item_secondPlace[MAX_PLR_NAMES];
    QTableWidgetItem *item_thirdPlace[MAX_PLR_NAMES];
    QTableWidgetItem *item_fourthPlace[MAX_PLR_NAMES];
    QTableWidgetItem *item_avgScore[MAX_PLR_NAMES];
    QTableWidgetItem *item_bestScore[MAX_PLR_NAMES];
    QTableWidgetItem *item_worstScore[MAX_PLR_NAMES];

    int gameStarted;
    int gameFinished;
    int handsPlayed;

    int firstPlace[MAX_PLR_NAMES];
    int secondPlace[MAX_PLR_NAMES];
    int thirdPlace[MAX_PLR_NAMES];
    int fourthPlace[MAX_PLR_NAMES];

    int totalScore[MAX_PLR_NAMES];
    int bestScore[MAX_PLR_NAMES];
    int worstScore[MAX_PLR_NAMES];


private: // functions
    void initVars();
    void createWindow();
    void updateWindow(int plr, int stats);

public: // functions
    bool isFileCorrupted();

    int  loadStatsFile();
    int  saveStatsFile();

    void increaseStats(int plr, int stats);
    void setScore(int plr, int score);
    void reset();
    void showStats();
    void Translate();
};

#endif // STATS_H
