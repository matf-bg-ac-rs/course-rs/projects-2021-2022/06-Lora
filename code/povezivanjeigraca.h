#ifndef POVEZIVANJEIGRACA_H
#define POVEZIVANJEIGRACA_H

#include <QDialog>

namespace Ui {
class PovezivanjeIgraca;
}

class PovezivanjeIgraca : public QDialog
{
    Q_OBJECT

public:
    explicit PovezivanjeIgraca(QWidget *parent = nullptr);
    ~PovezivanjeIgraca();

private slots:
    void inviteButton_clicked();
    void cancelButton_clicked();

private:
    Ui::PovezivanjeIgraca *ui;
    void posaljiInvite();
};

#endif // POVEZIVANJEIGRACA_H
