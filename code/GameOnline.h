#ifndef GAMEONLINE_H
#define GAMEONLINE_H
#include "Game.h"
#include "player.h"
#include <QList>
class GameOnline : public Game
{
public:
    GameOnline(QList<Player*> players);

//    void check();
//    bool checkLora();
    void gameHeart();
    void gameQueen();
    void gameLess();
    void gameMore();
    void gameJackClubLastHand();
    void gameKingHeartLastHand();
    void gameLora();
    void gameByChoice();
    void play();

};

#endif // GAMEONLINE_H
