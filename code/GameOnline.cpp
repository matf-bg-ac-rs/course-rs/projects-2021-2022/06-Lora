#include "GameOnline.h"


GameOnline::GameOnline(QList<Player*> players) : Game(players){}


void GameOnline::gameQueen(){
    Game::gameQueen();
}
void GameOnline::gameLess(){
    Game::gameLess();
}
void GameOnline::gameMore(){
    Game::gameMore();
}
void GameOnline::gameJackClubLastHand(){
    Game::gameJackClubLastHand();
}
void GameOnline::gameKingHeartLastHand(){
    Game::gameKingHeartLastHand();
}
void GameOnline::gameLora(){
    Game::gameLora();
}
void GameOnline::gameByChoice(){
    Game::gameByChoice();
}

void GameOnline::play(){
    Game::play();
}
