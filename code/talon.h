#ifndef TALON_H
#define TALON_H

#include <QObject>
#include "Card.h"
#include "player.h"
#include <QList>
#include <QPair>
class Talon:public QObject
{
    Q_OBJECT
public:
    QList<QPair<Card*, Player*>> getCardsOnTable();
    void addCard(Card* c, Player* plyr);
    int countCardsWithNumber(Card::Number num);
    int countCardsWithSuit(Card::Suit s);
    QPair<Card*, Player*> takeFromTop();
    Card::Suit getTalonSuit();
    bool empty();
private:
    QList<QPair<Card*, Player*>> cardsOnTable;
};
#endif // TALON_H
