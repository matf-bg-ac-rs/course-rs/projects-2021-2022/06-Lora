#include "GameVsComputer.h"

GameVsComputer::GameVsComputer(Player* player) : Game()
{

    Game::addPlayer(player);
    Game::addPlayer(new Computer(""));
    Game::addPlayer(new Computer(""));
    Game::addPlayer(new Computer(""));
}


void GameVsComputer::gameQueen(){
    Game::gameQueen();
}
void GameVsComputer::gameLess(){
    Game::gameLess();
}
void GameVsComputer::gameMore(){
    Game::gameMore();
}
void GameVsComputer::gameJackClubLastHand(){
    Game::gameJackClubLastHand();
}
void GameVsComputer::gameKingHeartLastHand(){
    Game::gameKingHeartLastHand();
}
void GameVsComputer::gameLora(){
    Game::gameLora();
}
void GameVsComputer::gameByChoice(){
    Game::gameByChoice();
}

void GameVsComputer::play(){

    Game::play();
}
