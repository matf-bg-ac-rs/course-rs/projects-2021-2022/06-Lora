#include "Card.h"

Card::Card(Card::Number num, Card::Suit s){
    number = num;
    suit = s;
}
unsigned int Card::getCardNumber(){
    return static_cast<unsigned int>(number);
}
Card::Suit Card::getCardSuit(){
    return suit;
}
