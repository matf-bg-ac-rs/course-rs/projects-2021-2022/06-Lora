#include "deck.h"
#include <qrandom.h>
#include <QRandomGenerator>
#include <algorithm>
Deck::Deck(){
    numberOfCards = 0;

    for(unsigned int s = static_cast<unsigned int>(Card::Suit::CLUB); s <= static_cast<unsigned int>(Card::Suit::SPADE); ++s){
        for(unsigned int num = static_cast<unsigned int>(Card::Number::SEVEN); num <= static_cast<unsigned int>(Card::Number::ACE); ++num){
            Card* card = new Card(static_cast<Card::Number>(num), static_cast<Card::Suit>(s));
            deck.append(card);
            numberOfCards+=1;
        }
    }

}
unsigned int Deck::getNumberOfCards(){
    return numberOfCards;
}
bool Deck::empty(){
    return numberOfCards == 0;
}
Card* Deck::takeCard(){
    numberOfCards -=1;
    return deck.takeLast();
}
void Deck::dealCards(QList<Player*> players){
    shuffleDeck();
    while(numberOfCards>0){

        for(unsigned int i = 0; i<=4; ++i){
            players[i]->takeCard(takeCard());
            players[i]->takeCard(takeCard());
        }


    }
}
void Deck::shuffleDeck(){

    std::random_shuffle(deck.begin(), deck.end());
}

QImage *Deck::getCardImage(int card) {
  if (card == backCard)
    return imageBackCard;

  assert((card >= 0) && (card < DECK_SIZE));
  return cardsImages[card];
}
