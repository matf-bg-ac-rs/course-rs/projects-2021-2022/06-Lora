#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QString>
#include <QVector>
#include <QtAlgorithms>
#include "Card.h"
#include "Game.h"
#include "talon.h"

class Game;
class Card;

class Player:public QObject{

    Q_OBJECT


public:
    Player();
    Player(QString name);
    void pickCard(int pos);
    Game::GAME pickGame();
    void takeCard(Card* card);
    void addPoints(unsigned int pts);
    void playCard();
    void removeCard(Card* card);
    QString getPlayerName();
    QVector<Card*> getCardsInHand();
    unsigned int getPoints();
//    QString getCardName();
    bool operator==(const Player* other);
    bool operator!=(const Player* other);
    void resetPoints();
    void reset();
    bool hasCard(Card* card);
    void sortCards();
    void setUserId(unsigned int id);
    unsigned int getUserId();
    int getNumOfCards();
    QVector<Card*> getCardsWithSuit(Card::Suit s);
    Card* getPickedCard();
    void resetPickedCard();
private:
    QString playerName;
    QVector<Card*> cardsInHand;
    unsigned int points;
    unsigned int userId;
    Card* pickedCard;


};

#endif // PLAYER_H
