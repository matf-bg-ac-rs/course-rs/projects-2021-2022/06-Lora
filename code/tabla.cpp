//#pragma once
#include "tabla.h"
#include "ui_tabla.h"
#include "qmainwindow.h"
#include "qwidget.h"
#include <QLabel>
#include <QFile>
#include <QDir>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QImage>
#include <QString>
#include <QMessageBox>
#include <QFlags>
#include <QTime>
#include <QResource>
#include <string.h>
#include "stats.h"
#include "time.h"
#include "assert.h"
#include <QTimer>
#include "positions.h"
#include "GameVsComputer.h"
#include "GameOnline.h"

void Tabla::on_actionNew_triggered() {
    player = new Player("You");
    lora = new GameVsComputer(player);
    lora->play();
}

void Tabla::on_actionConnect_triggered() {
    //ovde se radi povezivanje igraca
    lora = new GameOnline(players);
    lora->play();
}

void Tabla::on_labelKartaURuci1_clicked(){
    if(player->getNumOfCards()<1){
        ////////ispis greske trazi da ponovo bira
    }
    if(!checkCard(lora->getCurrentGame(), player->getCardsInHand()[0])){
        /////ispis greske mora ponovo da se bira karta
    }
    player->pickCard(0);
}
void Tabla::on_labelKartaURuci2_clicked(){
    if(player->getNumOfCards()<2){

    }
    if(!checkCard(lora->getCurrentGame(), player->getCardsInHand()[1])){
        /////ispis greske mora ponovo da se bira karta
    }
    player->pickCard(1);
}
void Tabla::on_labelKartaURuci3_clicked(){
    if(player->getNumOfCards()<3){

    }
    if(!checkCard(lora->getCurrentGame(), player->getCardsInHand()[2])){
        /////ispis greske mora ponovo da se bira karta
    }
    player->pickCard(2);
}
void Tabla::on_labelKartaURuci4_clicked(){
    if(player->getNumOfCards()<4){

    }
    if(!checkCard(lora->getCurrentGame(), player->getCardsInHand()[3])){
        /////ispis greske mora ponovo da se bira karta
    }
    player->pickCard(3);
}
void Tabla::on_labelKartaURuci5_clicked(){
    if(player->getNumOfCards()<5){

    }
    if(!checkCard(lora->getCurrentGame(), player->getCardsInHand()[4])){
        /////ispis greske mora ponovo da se bira karta
    }
    player->pickCard(4);
}
void Tabla::on_labelKartaURuci6_clicked(){
    if(player->getNumOfCards()<6){

    }
    if(!checkCard(lora->getCurrentGame(), player->getCardsInHand()[5])){
        /////ispis greske mora ponovo da se bira karta
    }
    player->pickCard(5);
}
void Tabla::on_labelKartaURuci7_clicked(){
    if(player->getNumOfCards()<7){

    }
    if(!checkCard(lora->getCurrentGame(), player->getCardsInHand()[6])){
        /////ispis greske mora ponovo da se bira karta
    }
    player->pickCard(6);
}
void Tabla::on_labelKartaURuci8_clicked(){
    if(player->getNumOfCards()<8){

    }
    if(!checkCard(lora->getCurrentGame(), player->getCardsInHand()[7])){
        /////ispis greske mora ponovo da se bira karta
    }
    player->pickCard(7);
}

Tabla::Tabla(QWidget *parent)
: QMainWindow(parent), ui(new Ui::Tabla) {
    ui->setupUi(this);
    centralWidget()->setStyleSheet("#centralWidget {"
                      "border-image: url(:/../resources/background/sto.png)"
                      " 0 0 0 0 stretch;}");

    ui->textEdit->setTextColor(Qt::white);
    ui->textEdit->setTextBackgroundColor(Qt::black);

    parent->setWindowTitle("Lora");

    config = new Config;

    lora = new Game();

    stats = new Stats;

    deck = new Deck;

    timer = new QTimer();

    //light_connected(false);
    connectedSetLights(false);

    // #TODO konektovanje

    setLanguage(config->getLanguage());

    message(tr("Welcome to ") + "Lora");

    if (stats->isFileCorrupted())
      message(tr("[Error]: The statistics file is corrupted!"));

}

Tabla::~Tabla() {
    delete ui;
    delete config;
    delete lora;
    delete stats;
    delete deck;
    delete timer;

    delete imageConnected;
    delete imageDisconnected;
    delete imageEmptyCard;
    delete imageSitHere;
    delete imageYourTurn;
}


void Tabla::initializeGraphics() {
    imageConnected = new QImage("../resource/icons/connected.png", "PNG");
    imageDisconnected = new QImage("../resources/icons/disconnected.png", "PNG");
    imageEmptyCard = new QImage("../resources/cards/1x/empty.png", "PNG");
    imageSitHere = new QImage("../resources/cards/1x/sithere.png", "PNG");
    imageYourTurn = new QImage("../resources/cards/1x/card-base2.png", "PNG");

    labels[0] = ui->labelKartaURuci1;
    labels[1] = ui->labelKartaURuci2;
    labels[2] = ui->labelKartaURuci3;
    labels[3] = ui->labelKartaURuci4;
    labels[4] = ui->labelKartaURuci5;
    labels[5] = ui->labelKartaURuci6;
    labels[6] = ui->labelKartaURuci7;
    labels[7] = ui->labelKartaURuci8;

    labels[8] = ui->labelKartaNaTalonuDole;
    labels[9] = ui->labelKartaNaTalonuLevo;
    labels[10] = ui->labelKartaNaTalonuGore;
    labels[11] = ui->labelKartaNaTalonuDesno;
    labels[12] = ui->labelIgracDole;
    labels[13] = ui->labelIgracLevo;
    labels[14] = ui->labelIgracGore;
    labels[15] = ui->labelIgracDesno;
    labels[16] = ui->labelOnline;
    labels[17] = ui->labelOnlineText;

    lcdScore[0] = ui->lcdBrojDole;
    lcdScore[1] = ui->lcdBrojLevo;
    lcdScore[2] = ui->lcdBrojGore;
    lcdScore[3] = ui->lcdBrojDesno;
}


void Tabla::connectedSetLights(bool connected) {
    if(connected)
        ui->labelOnlineText->setPixmap(QPixmap::fromImage(imageConnected->scaledToHeight(15)));
    else
        ui->labelOnlineText->setPixmap(QPixmap::fromImage(imageDisconnected->scaledToHeight(15)));
}


//void Tabla::gameOver(int score1, int score2, int score3, int score4) {
//  QString message1, message2;

//  stats->increaseStats(0, STATS_GAME_FINISHED);

//  clearDeck();

//  if (!lora->isItDraw())
//    message2 = tr("Won !");
//  else
//    message2 = tr("Draw !");

// int lowest = 0;
// int unsorted[4];

// unsorted[0] = score1;
// unsorted[1] = score2;
// unsorted[2] = score3;
// unsorted[3] = score4;

// for (int i = 0; i < 4; i++) {
//   int tmp = 0;
//   for (int j = 0; j < 4; j++)
//     if (unsorted[i] > unsorted[j])
//       tmp++;

////   switch(tmp) {
////     case 0 : stats->increaseStats(playerNames[i], STATS_FIRST_PLACE);
////              lowest = unsorted[i];
////              break;
////     case 1 : stats->increaseStats(playerNames[i], STATS_SECOND_PLACE);
////              break;
////     case 2 : stats->increaseStats(playerNames[i], STATS_THIRD_PLACE);
////              break;
////     case 3 : stats->increaseStats(playerNames[i], STATS_FOURTH_PLACE);
////              break;
////   }
// }

//// stats->setScore(playerNames[0], score1);
//// stats->setScore(playerNames[1], score2);
//// stats->setScore(playerNames[2], score3);
//// stats->setScore(playerNames[3], score4);

// message1 = tr("[Info]: GAME OVER!\n[Info]: Player '")  + labels[19]->text() + "': " +
//         QString::number(score1) + tr(" point(s) ") + (score1 == lowest ? message2 : "") +
//        tr("\n[Info]: Player '")                    + labels[20]->text() + "': " +
//         QString::number(score2) + tr(" point(s) ") + (score2 == lowest ? message2 : "") +
//        tr("\n[Info]: Player '")                    + labels[21]->text() + "': " +
//         QString::number(score3) + tr(" point(s) ") + (score3 == lowest ? message2 : "") +
//        tr("\n[Info]: Player '")                    + labels[22]->text() + "': " +
//         QString::number(score4) + tr(" point(s) ") + (score4 == lowest ? message2 : "");

// message(message1);


// QMessageBox::information(this, tr("Information"), message1, message2);

// ui->actionNew->setDisabled(false);
//}

//void Tabla::refreshScore(int score, int index) {
//  lcdScore[index]->display(score);
//}


//void Tabla::showDeck(Player* player, bool refresh) {
////  activeDeck = player;

//  if (onlineConnected) {
//    onlineShowDeck();
//    return;
//  }

//  int count = player->getNumOfCards() - 1;

//  for (int i = 0; i < 8; i++)
//    labels[0]->hide();

//  for (int i = 0; i < 8; i++) {
//    Card* card = player->getCardsInHand()[i];

//    if (card == nullptr)
//        break;

//    int pos = cardsPosition[count][i];

//    assert ((pos >= 0) && (pos <= 7));

//    labels[pos]->show();

//    labels[pos]->setPixmap(QPixmap::fromImage(deck->getImageCard(card)->scaledToHeight(cardHeight)));

//    if (player != lora->whoAmI())
//       labels[pos]->setDisabled(false);
//    else
//        if (refresh)
//            labels[pos]->setDisabled(!lora->isCardSelectable(card));
//  }
//}


void Tabla::setInfoChannel() {
 int newHeight = height();

 newHeight += ui->textEdit->height() + ui->lineEdit->height();
 if (newHeight > maxMainwindowHeight)
   newHeight = maxMainwindowHeight;

 setFixedHeight(newHeight);
 ui->textEdit->show();

}


void Tabla::clearDeck() {
  for (int i = 0; i < 8; i++)
    labels[i]->hide();
}

//void Tabla::clearTable() {
//  delay(200);
//  for (int i = 0; i < 4; i++) {
//    labels[14 + i]->setPixmap(QPixmap::fromImage(imageEmptyCard->scaledToHeight(cardHeight)));
////    cardPlayed[i] = empty;
//  }
//}

//void Tabla::selectCard(int num) {

//  if (onlineConnected) {
//    onlineSelectCard(num);
//    return;
//  }

//  if (activeDeck != lora->whoAmI()->getUserId())
//    return;

//  int cardID;
////TODO
//  cardID = reverseCardsPositions[lora->countMyCards() - 1][num];

//  assert((cardID >= 0) && (cardID <= 7));


//    ui->actionNew->setDisabled(true);
//    ui->actionConnect->setDisabled(true);

//    ui->actionNew->setDisabled(false);
//    ui->actionConnect->setDisabled(false);
//    //return;

//}


////void Tabla::playCard(int card, int index) {
//// delay(400);

//// cardPlayed[index] = card;
//// //koji br ovde ide??? bilo je 14
//// labels[8 + index]->setPixmap(QPixmap::fromImage(deck->getImageCard(card)->scaledToHeight(cardHeight)));
////}

////void Tabla::showYourTurn(int index) {
////  delay(200);

////  showDeck(activeDeck, true);
//////koji br ide ovde? bilo je 14
////  labels[8 + index]->setPixmap(QPixmap::fromImage(imageYourTurn->scaledToHeight(cardHeight)));
////  cardPlayed[index] = yourTurn;

////  if (!onlineConnected) {
////    ui->actionNew->setDisabled(false);
////    ui->actionConnect->setDisabled(false);
////  }
////}

void Tabla::setCardsDisabled() {
 for (int i = 0; i < 8; i++)
   labels[i]->setDisabled(true);
}


void Tabla::message(QString msg) {
  ui->textEdit->append(msg);
}

//void Tabla::on_labelKartaURuci1_clicked() {
//  selectCard(0);
//}

//void Tabla::on_labelKartaURuci2_clicked() {
//  selectCard(1);
//}

//void Tabla::on_labelKartaURuci3_clicked() {
//  selectCard(2);
//}

//void Tabla::on_labelKartaURuci4_clicked() {
//  selectCard(3);
//}

//void Tabla::on_labelKartaURuci5_clicked() {
//  selectCard(4);
//}

//void Tabla::on_labelKartaURuci6_clicked() {
//  selectCard(5);
//}

//void Tabla::on_labelKartaURuci7_clicked() {
//  selectCard(6);
//}

//void Tabla::on_labelKartaURuci8_clicked() {
//  selectCard(7);
//}



void Tabla::on_actionRules_triggered() {
    help->show();
}


//////////////////OVO SREDITI
void Tabla::on_actionReset_triggered()
{
  QMessageBox messageBox(this);
  messageBox.setText(tr("Do you want to reset statistics?"));
  messageBox.setInformativeText(tr("Are you sure?"));
  messageBox.addButton(QMessageBox::Yes);
  messageBox.addButton(QMessageBox::No);
  messageBox.setDefaultButton(QMessageBox::No);
  int ret = messageBox.exec();
  if (ret == QMessageBox::Yes) {
     stats->reset();
     message(tr("[Info]: You've resetted the statistics!"));
  }
}

void Tabla::on_actionShow_triggered() {
  stats->show();
}

void Tabla::setLanguage(int lang) {
 ui->actionEnglish->setChecked(false);
 ui->actionSrpski->setChecked(false);

 qApp->removeTranslator(&translator);

 switch (lang) {
    case LANG_ENGLISH: if (translator.load(QLocale(QLocale::English), QLatin1String("translation"), QLatin1String("_"), QLatin1String(":/languages"))) {
                         qApp->installTranslator(&translator);

                         ui->actionEnglish->setChecked(true);
                         }
     break;
case LANG_SRPSKI:  if (translator.load(QLocale(QLocale::Serbian), QLatin1String("translation"), QLatin1String("_"), QLatin1String(":/languages"))) {
       qApp->installTranslator(&translator);

       ui->actionSrpski->setChecked(true);

       }
       break;

}

ui->retranslateUi(this);
stats->Translate();
}

void Tabla::on_actionEnglish_triggered() {
  ui->actionEnglish->setChecked(true);

  setLanguage(LANG_ENGLISH);

  config->setLanguage(LANG_ENGLISH);
}

void Tabla::on_actionSrpski_triggered() {
  ui->actionSrpski->setChecked(true);

  setLanguage(LANG_SRPSKI);

  config->setLanguage(LANG_SRPSKI);
}

void Tabla::on_lineEdit_returnPressed() {
  QString data;



  data += ui->lineEdit->text();

  //client->send(data);
  message(data);

  ui->lineEdit->clear();
}

//void Tabla::on_actionConnect_triggered()
//{
//  bool warning = config->Warning();

//  Connect connectDiag(this);
//  connectDiag.setModal(true);
//  connectDiag.config(config->Username(), config->Password(), warning);

//  connectDiag.exec();

//  client->setHandle(connectDiag.getHandle());
//  client->setPassword(connectDiag.getPassword());

//  config->setOnline(connectDiag.getHandle(), connectDiag.getPassword());

//  if (warning && connectDiag.WarningDisabled())
//    config->setConfigFile(CONFIG_WARNING, false);

//  if (connectDiag.WarningAccepted())
//    switch (connectDiag.result()) {
//      case QDialog::Accepted: client->socketConnect(true, connectDiag.getHost(), connectDiag.getPort());
//                              break;
//      case QDialog::Rejected:
//                              break;
//      default: client->socketConnect(false, connectDiag.getHost(), connectDiag.getPort());
//    }
//}



//void Tabla::resetCardsPos()
//{
//  for (int i = 0; i < 8; i++)
//    labels[i]->move(labels[i]->x(), defCardPosY);
//}

//void Tabla::joinGame(int id, char chair)
//{
//  QString m;

//  if (id == onlineTableID) {
//    if (chair != ' ')
//      m.sprintf("%c, sit", chair);
//    else
//      return;
//  } else {
//      if (chair == ' ')
//        m.sprintf("%d, join", id);
//      else
//        m.sprintf("%d %c, join", id, chair);
//    }
//  client->send(m);
//}








